Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

This is different from the log to the console because it allows us to choose to report here and have error and warning messages that persist instead of just writing them to the console. So instead of the messages having to be looked at in the moment after the program was run by scrolling up and down the console, we can look back here and retrieve and/or analyze the messages at a later time.

1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

This line comes from JUnit evaluating the DisabledCondition for a test that is not disabled.

1.  What does Assertions.assertThrows do?

It asserts that the method throws an exception of the expectedType (first parameter) and it returns the exception. If no exception is thrown or an excpetion different from the expectedType is thrown, it fails. 

1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)

    It is a version number for a serializable class. It it used during deserialization to verify that the sender and receiver of a object have loaded classes that are compatible. That way, if the receiver loaded a class that has a different serialVersionUID, then deserialization will result in an InvalidClassException. If we do not explicitly give a serializable class one, then there will be one calculated in runtime, but it is best to declare it explicitly, because the calculation of one if compiler dependent (sensitive to class details that can differ depending on the compiler implementation), so it can result in unexpected InvalidClassExceptions. It matters greatly whenever you must use deserialization and represents a class version. It should also be updated if you develop the class to a point where it is not backwards compatible with the previous version.

    2.  Why do we need to override constructors?

    We need to override them because we wanted to be able to construct the Exception with a String message or a String message and a Throwable cause instead of just the normal, lacking-arguments constructor. We had to override them because if we want to set the cause or the message, the only way to do so is to construct the superclass with the parameters included. We couldn't have constructed the exception and then added these.

    3.  Why we did not override other Exception methods?	

    Because we did not need to change functionality of any of the other exception methods; with them, it is not a case like above where the only way to get an exception with data is to construct it with it. 

1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

In Java, static blocks can be used for static initializations of a class; it is a block that executes once, the first time the class is loaded into memory. In this particular case, the static block creates a configuration file from the logger.properties and loads the class and passes the configuration file to the LogManager, which reads and initializes the default configuration. It then sends to the logger a message that the app is starting. 

1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

README.md uses a file format called Markdown, which is a language used to create rich text from a text editor that can be edited in a readable format. README files are used to contain documentation about the directory / software / archive. Usually they are helpful to the user or for others to help contribute to an open-source project. Bitbucket is a hosting service which, like GitHub, uses README files and displays them on the main page of every repository. 

1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

The "Test Timer fail" failOverTest() is failing because the test asserts that the timeMe(-1) function should throw a TimerException but is instead throwing a NullPointerException. 

1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

This was happening because the if statement runs, which throws a 
	new TimerException("Cannot be less than zero"); 
and then the finally block runs
	logger.info("Calling took: "+ (System.currentTimeMillis() - timeNow));
	logger.info("* should take: "+ timeToWait);
and because the if statement threw an exception, the code right below it in the try block, that set timeNow, never ran, so timeNow was never initialized at the point where it is attempted to be subtracted from the currentTimeMillis()

1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 

![JUnit5 Print Screen](/JUnit_PrintScreen.png)

1.  Make a printScreen of your eclipse Maven test run, with console

![JUnit5 Print Screen](/Maven_PrintScreen.png)

1.  What category of Exceptions is TimerException and what is NullPointerException

NullPointerException is a RunTimeException, and TimerException is a subclass of Exception, and it is considered a checked exemption.

1.  Push the updated/fixed source code to your own repository.

Done.


